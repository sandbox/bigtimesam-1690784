jQuery(document).ready(function() {
  jQuery("#domains-sortable")
  .tablesorter( { widthFixed: true, headers: { 1: { sorter: false } }, widgets: ['zebra'], sortList: [[0, 0]] } );
  jQuery("#entries-sortable")
  .tablesorter( { widthFixed: true, headers: { 4: { sorter: false } }, widgets: ['zebra'], sortList: [[0, 0]] } )
});
