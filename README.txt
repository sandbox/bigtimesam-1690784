This module can be used to control BIND with a MySQL backend: http://mysql-bind.sourceforge.net/.

Database structure must be as follows:
  <database name>
    - domain_com
      (id, name, ttl, rdtype, rdata)
        id = auto_increment
    - foobar_com
      (same fields as above)
    etc.
