<?php

function bind_mysql_control_panel_overview_form() {

  $link = _bind_mysql_control_panel_init_db();
  _bind_mysql_control_panel_select_db($link);

  $header = array(t('Domain name'), array('data' => t('Operations'), 'colspan' => 3));
  $rows = array();

  if( $link ) {
    $result = mysql_query("SHOW TABLES FROM " . addslashes(variable_get('bind_mysql_control_panel_db_db')), $link);
    if( $result ) {
      while ($obj = mysql_fetch_row($result) ) {
        $domain = str_replace("_", ".", $obj[0]);
        $row = array(
          check_plain($domain),
          l(t('edit'), "admin/config/services/bind/edit/$obj[0]"),
          //l(t('delete'), "admin/config/services/bind/delete/$obj[0]"),
        );
        $rows[] = $row;
      }
    }
  }

  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'domains-sortable')));
  $form = drupal_get_form("bind_mysql_control_panel_advanced_actions_form");

  $output .= drupal_render($form);

  return $output;
}

function bind_mysql_control_panel_advanced_actions_form() {

  $form = array();

  $form['advanced_actions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced actions'),
    '#attributes' => array('class' => array('container-inline')),
  );
  $form['advanced_actions']['action'] = array(
    '#type' => 'select',
    '#title' => t('Action'),
    '#title_display' => 'invisible',
    '#options' => array("reload" => "Reload BIND"),
    '#empty_option' => t('Choose an advanced action'),
  );
  $form['advanced_actions']['actions'] = array('#type' => 'actions');
  $form['advanced_actions']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Run'),
  );

  return $form;

}

function bind_mysql_control_panel_advanced_actions_form_submit($form, &$form_state) {
  if( isset($form['advanced_actions']['action']['#value']) && strlen($form['advanced_actions']['action']['#value']) > 0 ) {
    switch( $form['advanced_actions']['action']['#value'] ) {
      case "reload":
        if( variable_get('bind_mysql_control_panel_poll_url') != "" ) {
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, variable_get('bind_mysql_control_panel_poll_url'));
          curl_setopt($ch, CURLOPT_HEADER, 0);
          curl_exec($ch);
          curl_close($ch);
          drupal_set_message(t('BIND reload successfully queued.'));
        }
        else {
          drupal_set_message(t('Poll URL is not set. Go to !link to set it.', array('!link' => l(t('settings'), 'admin/config/services/bind/settings'))), 'warning');
        }
        break;
     }
  }
  else {
    drupal_set_message(t('Please select an action.'), 'warning');
  }
}

/**
 * Form callback: builds the form for settings page.
 *
 * @param $form
 *   An associative array containing the structure of the form.
 * @param $form_state
 *   An associative array containing the current state of the form.
 *
 * @return
 *   An array representing the form definition.
 *
 * @ingroup forms
 * @see bind_mysql_control_panel_add_domain_form_validate()
 * @see bind_mysql_control_panel_add_domain_form_submit()
 */
function bind_mysql_control_panel_settings_form($form, &$form_state) {

  $form['database'] = array(
    '#type' => 'fieldset',
    '#title' => t('Database settings'),
    '#collapsible' => TRUE,
  );
  $form['database']['type'] = array(
    '#type' => 'select',
    '#title' => t('Database type'),
    '#options' => array("mysql" => "MySQL"),
    '#default_value' => "mysql",
    '#required' => TRUE    
  );
  $form['database']['hostname'] = array(
    '#type' => 'textfield',
    '#title' => t('Database server hostname'),
    '#default_value' => variable_get('bind_mysql_control_panel_db_host'),
    '#required' => TRUE
  );
  $form['database']['database'] = array(
    '#type' => 'textfield',
    '#title' => t('Database name'),
    '#default_value' => variable_get('bind_mysql_control_panel_db_db'),
    '#required' => TRUE
  );
  $form['database']['username'] = array(
    '#type' => 'textfield',
    '#title' => t('Database username'),
    '#default_value' => variable_get('bind_mysql_control_panel_db_user'),
    '#required' => TRUE
  );
  $form['database']['password'] = array(
    '#type' => 'password',
    '#title' => t('Database password'),
    '#description' => t('Enter a new password to change your current password.'),
  );

  $form['other'] = array(
    '#type' => 'fieldset',
    '#title' => t('Other settings'),
    '#collapsible' => TRUE,
  );
  $form['other']['poll_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Primary NS poll URL'),
    '#description' => t('Insert your name server poll URL, e.g. <i>http://domain.tld/update-rndc.php</i>.'),
    '#default_value' => variable_get('bind_mysql_control_panel_poll_url'),
    '#required' => FALSE,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
  );

  return $form;
}

/**
 * Validation handler for bind_mysql_control_panel_add_domain_form().
 */
function bind_mysql_control_panel_settings_form_validate($form, &$form_state) {
  // Check if given poll url is not valid
  if( $form['other']['poll_url']['#value'] != "" && !preg_match('/http(s?):\/\/(.*).(.*)/', $form['other']['poll_url']['#value']) )
    form_set_error('poll_url', t('Poll URL is not valid.'));
}

/**
 * Submit handler for bind_mysql_control_panel_add_domain_form().
 */
function bind_mysql_control_panel_settings_form_submit($form, &$form_state) {
  variable_set('bind_mysql_control_panel_db_host', $form['database']['hostname']['#value']);
  variable_set('bind_mysql_control_panel_db_db', $form['database']['database']['#value']);
  variable_set('bind_mysql_control_panel_db_user', $form['database']['username']['#value']);
  if( strlen( trim($form['database']['password']['#value']) ) > 0 )
    variable_set('bind_mysql_control_panel_db_pass', $form['database']['password']['#value']);
  variable_set('bind_mysql_control_panel_poll_url', $form['other']['poll_url']['#value']);

  drupal_set_message(t('The configuration options have been saved.'));
  $form_state['redirect'] = 'admin/config/services/bind/';
}

/**
 * Form callback: builds the form for adding a new domain.
 *
 * @param $form
 *   An associative array containing the structure of the form.
 * @param $form_state
 *   An associative array containing the current state of the form.
 *
 * @return
 *   An array representing the form definition.
 *
 * @ingroup forms
 * @see bind_mysql_control_panel_add_domain_form_validate()
 * @see bind_mysql_control_panel_add_domain_form_submit()
 */
function bind_mysql_control_panel_add_domain_form($form, &$form_state) {
  $form['domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Domain'),
    '#description' => t('Insert domain name, e.g. <i>domain.tld</i>.'),
    '#required' => TRUE,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Insert new domain'),
  );

  return $form;
}

/**
 * Validation handler for bind_mysql_control_panel_add_domain_form().
 */
function bind_mysql_control_panel_add_domain_form_validate($form, &$form_state) {
  // Check if given domain name is not valid
  if( !preg_match('/^([a-zA-Z0-9-]+)\.[a-zA-Z]{2,6}$/', $form_state['values']['domain']) )
    form_set_error('domain', t('Domain name %domain is not valid. Please enter a valid domain name.', array('%domain' => $form_state['values']['domain'])));
}

/**
 * Submit handler for bind_mysql_control_panel_add_domain_form().
 */
function bind_mysql_control_panel_add_domain_form_submit($form, &$form_state) {
  // Save a new shortcut set with links copied from the user's default set.
  $domain = (object) array(
    'domain' => $form_state['values']['domain'],
    'domain_db' => str_replace(".", "_", $form_state['values']['domain'])
  );
  bind_mysql_control_panel_save_domain($domain);
  $form_state['redirect'] = 'admin/config/services/bind/' . $domain->domain_db;
}

/**
 * Menu callback; Display a text format form.
 */
function bind_mysql_control_panel_add_entry_form($domain = NULL) {
  $form = drupal_get_form('bind_mysql_control_panel_entry_form', 'add', $domain, null);
  return drupal_render($form);
}

function bind_mysql_control_panel_fix_database_form($form, &$form_state) {
  $question = t('Do you want to fix your database structure?');
  $return = "admin/config/services/bind/";
  
  return confirm_form($form,
    $question,
    $return,
    t('<b>Unfortunately your database is not in a supported format.</b> Each row is required to have a unique id.<br><br>
      To fix this problem, the following SQL query must be run for each domain:<blockquote>ALTER TABLE `domain_tld` ADD `id` INT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`)</blockquote>
      You can authorize this tool to alter your database by clicking "Run".'),
    t('Run'), t('Cancel'));
}

function bind_mysql_control_panel_fix_database_form_submit($form, &$form_state) {
  _bind_mysql_control_panel_check_db(false);
  drupal_goto("admin/config/services/bind/");
}

function bind_mysql_control_panel_delete_entry_form($form, &$form_state, $entry) {
  $form['_entry'] = array('#type' => 'value', '#value' => $entry);
  $question = t('Are you sure you want to delete this entry?');
  $return = "admin/config/services/bind/";
  
  if( preg_match("/(.*)-(\d+)/", $entry, $matches) )
    $return = "admin/config/services/bind/edit/" . $matches[1];

  return confirm_form($form,
    $question,
    $return,
    t('This action cannot be undone.'),
    t('Delete entry'), t('Cancel'));
}

function bind_mysql_control_panel_delete_entry_form_submit($form, &$form_state) {
  $link = _bind_mysql_control_panel_init_db();
  _bind_mysql_control_panel_select_db($link);

  if( isset($form['_entry']) ) {
    $parameters = explode("-", $form['_entry']['#value']);
    $result = mysql_query("DELETE FROM " . addslashes($parameters[0]) . " WHERE id = " . addslashes($parameters[1]));
    if( !$result )
      drupal_set_message(t('Unable to delete entry') . ": " . mysql_error(), 'error');
    else
      drupal_set_message(t('Entry deleted.'));

    drupal_goto("admin/config/services/bind/edit/" . $parameters[0]);
  }
  else {
    drupal_set_message(t('Unable to delete entry') . '.');
    drupal_goto("admin/config/services/bind/");
  }
}

/**
 * Menu callback; Display a text format form.
 */
function bind_mysql_control_panel_admin_domain_page($domain = NULL) {

  $link = _bind_mysql_control_panel_init_db();
  _bind_mysql_control_panel_select_db($link);

  // If parameter is "domain_com-<int>", then we are editing an entry.
  $parameters = explode("-", $domain);

  if( !isset($parameters[1]) ) {
    $header = array(t('Name'), t('TTL'), t('RDTYPE'), t('RDATA'), array('data' => t('Operations'), 'colspan' => 2));
    $rows = array();

    $domain = addslashes($domain);

    $result = mysql_query("SELECT * FROM " . $domain . " ORDER BY name ASC", $link);

    if( $result ) {
      while ($obj = mysql_fetch_object($result) ) {
        $row = array(
          check_plain($obj->name),
          check_plain($obj->ttl),
          check_plain($obj->rdtype),
          check_plain($obj->rdata),
          l(t('edit'), "admin/config/services/bind/edit/$domain-$obj->id"),
          l(t('delete'), "admin/config/services/bind/delete/$domain-$obj->id"),
        );
        $rows[] = $row;
      }
    }

    return theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'entries-sortable')));
  }

  // Editing a previous entry record
  else {
    $result = mysql_query("SELECT * FROM " . addslashes($parameters[0]) . " WHERE id = " . addslashes($parameters[1]));
    if( $result ) {
      if( mysql_num_rows($result) == 1 ) {
        $obj = mysql_fetch_object($result);
        $form = drupal_get_form('bind_mysql_control_panel_entry_form', 'edit', $parameters[0], $parameters[1]);
        $form['name']['#value'] = $obj->name;
        $form['ttl']['#value'] = $obj->ttl;
        $form['rdtype']['#value'] = array_search($obj->rdtype, variable_get('bind_mysql_control_panel_rdtypes'));
        $form['rdata']['#value'] = $obj->rdata;
        $form['mode'] = array("#type" => "value", "#value" => "edit");
        return drupal_render($form);
      }
    }
  }
}

function bind_mysql_control_panel_entry_form($form, &$form_state) {

  $form = array();

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Hostname, e.g. <i>domain.tld</i>.'),
    '#required' => TRUE,
  );

  $form['ttl'] = array(
    '#type' => 'select',
    '#title' => t('TTL'),
    '#options' => variable_get('bind_mysql_control_panel_ttls'),
    '#default_value' => "3600",
    '#description' => t('TTL (time-to-live), default value <i>1 hour</i>.'),
    '#required' => TRUE,
  );

  $form['rdtype'] = array(
    '#type' => 'select',
    '#title' => t('RDTYPE'),
    '#options' => variable_get('bind_mysql_control_panel_rdtypes'),
    '#description' => t('DNS record type, e.g. <i>CNAME</i>.'),
    '#required' => TRUE,
  );

  $form['rdata'] = array(
    '#type' => 'textfield',
    '#title' => t('RDATA'),
    '#description' => t('Field value, e.g. <i>127.0.0.1</i>'),
    '#required' => TRUE,
  );

  $form['mode'] = array(
    '#type' => 'value',
    '#value' => $form_state['build_info']['args'][0]
  );

  $form['domain'] = array(
    '#type' => 'value',
    '#value' => $form_state['build_info']['args'][1]
  );

  if( isset($form_state['build_info']['args'][2]) ) {
    $form['entry-id'] = array(
      '#type' => 'value',
      '#value' => $form_state['build_info']['args'][2]
    );
  }

  $form['redirect'] = array(
    '#type' => 'value',
    '#value' => 'admin/config/services/bind/edit/' . $form_state['build_info']['args'][1]
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
  );

  $form['#submit'][] = 'bind_mysql_control_panel_entry_form_submit';

  return $form;
}

/**
 * Validation handler for bind_mysql_control_panel_add_domain_form().
 */
function bind_mysql_control_panel_entry_form_validate($form, &$form_state) {

  $mode = addslashes($form['mode']['#value']);
  $domain = addslashes($form['domain']['#value']);

  // Validity checks when adding a new entry
  if( $mode == "add" ) {
    // Check if zone already has a SOA record
    $rdtypes = variable_get('bind_mysql_control_panel_rdtypes');
    if( $rdtypes[$form['rdtype']['#value']] == "SOA" ) {
      $link = _bind_mysql_control_panel_init_db();
      _bind_mysql_control_panel_select_db($link);
      $result = mysql_query("SELECT id FROM $domain WHERE rdtype = 'SOA'");
      if( $result && mysql_num_rows($result) > 0 ) {
        $row = mysql_fetch_object($result);
        form_set_error('rdtype', t('This zone already has a ') . l(t('SOA record'), "admin/config/services/bind/edit/$domain-$row->id") . '.');
      }
    }
  }
}

function bind_mysql_control_panel_entry_form_submit($form, &$form_state) {
  $link = _bind_mysql_control_panel_init_db();
  _bind_mysql_control_panel_select_db($link);

  $mode = addslashes($form['mode']['#value']);
  $domain = addslashes($form['domain']['#value']);

  $name = addslashes($form['name']['#value']);
  $ttl = addslashes($form['ttl']['#value']);
  $rdtypes = variable_get('bind_mysql_control_panel_rdtypes');
  $rdtype = $rdtypes[$form['rdtype']['#value']];
  $rdata = addslashes($form['rdata']['#value']);

  if( $mode == "add" ) {
    $result = mysql_query("INSERT INTO $domain (name, ttl, rdtype, rdata) VALUES ('$name', '$ttl', '$rdtype', '$rdata')");
    if( !$result ) {
      drupal_set_message("Unable to create a new entry: " . mysql_error(), "error");
    }
    else {
      if( $rdtype != "SOA" )
        update_soa_field($domain);
      drupal_set_message(t('Entry has been created.'));
    }
  }
  else {
    $entry_id = addslashes($form['entry-id']['#value']);
    $result = mysql_query("UPDATE $domain SET name = '$name', ttl = '$ttl', rdtype = '$rdtype', rdata = '$rdata' WHERE id = '$entry_id'");
    if( !$result ) {
      drupal_set_message(t('Unable to edit entry: ') . mysql_error(), "error");
    }
    else {
      if( $rdtype != "SOA" )
        update_soa_field($domain);
      drupal_set_message(t('Entry has been updated.'));
    }
  }

  drupal_goto( $form['redirect']['#value'] );
}

function update_soa_field($domain) {

  $link = _bind_mysql_control_panel_init_db();
  _bind_mysql_control_panel_select_db($link);

  $result = mysql_query("SELECT id, rdata FROM " . $domain . " WHERE rdtype = 'SOA'");

  if( $result && mysql_num_rows($result) == 1 ) {
    $row = mysql_fetch_object($result);
    $serial = $row->rdata;
    preg_match("/(.*) (\d{8})(\d{2}) (.*)/", $serial, $matches);

    if( count($matches) > 4 ) {
      $timestamp = $matches[2];
      $counter = $matches[3];
      $new_serial = null;

      if( $timestamp == date("Ymd") ) {
        if( is_numeric($counter) )
          $counter++;
        else
          $counter = "01";
        $new_serial = date("Ymd") . str_pad($counter, 2, "0", STR_PAD_LEFT);
      }
      else {
        $new_serial = date("Ymd") . "01";
      }

      if( is_numeric($new_serial) ) {
        $new_serial_row = $matches[1] . " " . $new_serial . " " . $matches[4];
        $update = mysql_query("UPDATE " . $domain . " SET rdata = '$new_serial_row' WHERE rdtype = 'SOA'");
        if( !$update )
          drupal_set_message(t('Unable to update SOA field: ') . mysql_error(), 'warning');
      }
    }
  }
  else {
    drupal_set_message(t('Unable to update SOA field.'), 'warning');
  }
}

function _bind_mysql_control_panel_init_db() {

  if( !variable_get('bind_mysql_control_panel_db_host') || !variable_get('bind_mysql_control_panel_db_db') || !variable_get('bind_mysql_control_panel_db_user') ) {
    drupal_set_message(t('Please insert your database crendentials.'), "warning");
    drupal_goto("admin/config/services/bind/settings");
  }

  $link = @mysql_connect(variable_get('bind_mysql_control_panel_db_host'), variable_get('bind_mysql_control_panel_db_user'), variable_get('bind_mysql_control_panel_db_pass'));

  if (!$link) {
    if( current_path() != "admin/config/services/bind/settings" ) {
      drupal_set_message(t('Database error: ') . mysql_error(), "error");
      drupal_set_message(t('You have to fix your database settings before you can use this tool.'), "warning");
      drupal_goto("admin/config/services/bind/settings");
    }
  }
  else {
    return $link;
  }
}

function _bind_mysql_control_panel_select_db($link) {
  if( $link ) {
    $db_selected = mysql_select_db(variable_get('bind_mysql_control_panel_db_db'), $link);
    if (!$db_selected) {
      if( current_path() != "admin/config/services/bind/settings" ) {
        drupal_set_message(t('Unable to select database: ') . mysql_error(), "error");
        drupal_set_message(t('You have to fix your database settings before you can use this tool.'), "warning");
        drupal_goto("admin/config/services/bind/settings");
      }
    }
    else {
      _bind_mysql_control_panel_check_db();
      return true;
    }
  }
  return false;
}

function _bind_mysql_control_panel_check_db($ask = true) {

  $link = @mysql_connect(variable_get('bind_mysql_control_panel_db_host'), variable_get('bind_mysql_control_panel_db_user'), variable_get('bind_mysql_control_panel_db_pass'));
  mysql_select_db(variable_get('bind_mysql_control_panel_db_db'), $link);

  $result = mysql_query("SHOW TABLES FROM bind", $link);

  if( $result ) {
    while( $obj = mysql_fetch_row($result) ) {
      $res = mysql_query('DESCRIBE ' . addslashes($obj[0]) );
      $key = false;
      while($row = mysql_fetch_array($res)) {
        if( $row['Field'] == "id" )
          $key = true;
      }
      if( !$key ) {
        if( $ask )
          drupal_goto("admin/config/services/bind/fix-database");
        mysql_query("ALTER TABLE `" . addslashes($obj[0]) . "` ADD `id` INT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`)");
        drupal_set_message(t('Table $obj[0] has been automatically fixed.'));
        variable_set('bind_mysql_control_panel_db_ok', true);
      }
    }
  }
}
